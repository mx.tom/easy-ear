import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import SoundFont from 'react-native-soundfont';
import CheckBox from '@react-native-community/checkbox';
import { Component } from 'react';


const chordFunctions = ["I", "ii", "iii", "IV", "V", "vi"]
const chordFunctionsFull = {"I": [[0, 4, 7], [4, 7, 12], [7, 12, 16], [-5, 4, 7]],
                            "ii": [[2, 5, 9], [5, 9, 14], [9, 14, 17], [-3, 2, 5]],
                            "iii":[[4, 7, 11], [7, 11, 16], [-1, 4, 7], [-5, -1, 4]],
                            "IV": [[5, 9, 12], [9, 12, 17], [0, 5, 9], [-3, 0, 5]],
                            "V": [[7, 11, 14], [-5, -1, 2], [-1, 2, 7], [2, 7, 11]],
                            "vi": [[9, 12, 16], [-3, 0, 4], [0, 4, 9], [4, 9, 12]]}
const notes = ["G2", "AB2", "A2", "BB2", "B2", "C3", "DB3", "D3", "EB3", "E3", "F3", "GB3", "G3", "AB3", "A3", "BB3", "B3", "C4", "DB4", "D4", "EB4", "E4", "F4", "GB4", "G4", "AB4", "A4", "BB4", "B4", "C5", "DB5", "D5", "EB5", "E5", "F5", "GB5", "G5", "AB5"]
const keys = ["Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B", "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B", "C", "Db", "D"]

var chordProgression = ["I"]
var chordList = [["A3", "DB4", "E4"], ["B3", "D4", "GB4"], ["D4", "GB4", "A4"], ["GB4", "A4", "DB5"], ["B3", "D4", "GB4"]]
var uniqueNotes = []
const progressionLength = 4

const instrument = SoundFont.instrument('acoustic_grand_piano', {
  notes: notes,
  gain: 3,
  duration: 0.5,
}).then((piano) => {
  return piano
});

const timer = ms => new Promise(res => setTimeout(res, ms));

export class Player {
  constructor(chordProgressionStr, chordList) {
    this.chordProgressionStr = chordProgressionStr;
    this.chordNotes = chordList;
    this.state = {
      stop: false,
    }
    console.log("Constructing new player")
  }

  async play(){
    for (let ij = 0; ij < 5; ij++) {
      if (this.state.stop == true) {
        return;
      }
      else {
        console.log("Play", chordList[ij])
        for (let ih = 0; ih < 3; ih++) {
            instrument.then((piano) => { piano.start(this.chordNotes[ij][ih], 0) });
          }
      }
      await timer(1100);
    }
  }

  stop(){
    this.state.stop = true;
  }
}

// Create a player once at the initialization of the app
var myPlayer = new Player(chordProgression, chordList);

export default function App() {

  const [ProgressionText, setProgressionText] = useState('');
  const [key, setKey] = useState('');
  const [inversion, setInversion] = useState(false);

  function playProgression(chordProgression, chordList){
    myPlayer.stop();
    myPlayer = new Player(chordProgression, chordList);
    myPlayer.play();
  }

  // min and max included
  function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  function chord(){
    chordProgression = ["I"]
    var progressionNotes = []
    var progressionNoteIndex = []
    const referenceNote = randomInteger(0, 2)
    var noteIndex = randomInteger(8, 20)
    setKey(keys[noteIndex-8])
    progressionNotes = [notes[noteIndex], notes[noteIndex+4], notes[noteIndex+7]]
    var firstNoteList = [noteIndex, noteIndex+4, noteIndex+7]
    progressionNoteIndex = [firstNoteList[referenceNote]]
    chordList = [[notes[noteIndex], notes[noteIndex+4], notes[noteIndex+7]]]

    for (let i = 0; i < progressionLength; i++){

      // Make sure the new chord is not the same chord as the last chord
      let newChord = chordProgression[chordProgression.length-1]
      while (newChord == chordProgression[chordProgression.length-1]) {
        const func = randomInteger(1, 5);
        newChord = chordFunctions[func]
      }

      chordProgression.push(newChord);

      // todo cleanup
      // use inversion if activated
      var inversionIndex = 0
      
      if (inversion == true){
        var diff = 100
        var oldhigh = progressionNoteIndex[progressionNoteIndex.length-1]

        for (let i = 0; i < 4; i++) {
          
          var newhigh = chordFunctionsFull[newChord][i][referenceNote] + noteIndex
          // console.log("newhigh ", newhigh)
          var newdiff = Math.abs(oldhigh-newhigh)
          // console.log(newdiff)
          if ( newdiff < diff) {
            diff = newdiff
            inversionIndex = i
          }
        }
        console.log("inversion: ", inversionIndex)
      }
      var chordNotes = chordFunctionsFull[newChord][inversionIndex]
      progressionNoteIndex.push(noteIndex+chordNotes[referenceNote])
      progressionNotes.push(notes[noteIndex+chordNotes[0]], notes[noteIndex+chordNotes[1]], notes[noteIndex+chordNotes[2]])
      chordList.push([notes[noteIndex+chordNotes[0]], notes[noteIndex+chordNotes[1]], notes[noteIndex+chordNotes[2]]])
    }
    
    uniqueNotes = [...new Set(progressionNotes)]
    console.log("New Progression: ", chordProgression)
    console.log("Chords: ", chordList)
  }

  function next(){
    setProgressionText("");
    chord();
    playProgression(chordProgression, chordList);
  }

  // Call this only once when loading the application
  useEffect(() => {
    chord();
  }, []);

  const clickHandlerNext = () => {
    next();
  };

  const clickHandlerReveal = () => {
    setProgressionText(chordProgression.join("  "));
  };

  const clickHandlerRepeat = () => {
    playProgression(chordProgression, chordList);
  };
  
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Text style={styles.keyText}>Key: {key}</Text>

        <View style={styles.inversionContainer}>
          <Text style={styles.inversionText}>Inversions: </Text> 
          <CheckBox 
            style={styles.inversionCheck} 
            value={inversion} 
            onValueChange={setInversion}           
            tintColors={{true: '#b80050'}}
          />
        </View>
      </View>
      

      <View style={styles.containerProgression}>
        <Text style={styles.progressionText}>{ProgressionText}</Text>
      </View>

      <View style={styles.buttonRow}>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.btnBG}
            onPress={clickHandlerRepeat}>
            <Text style={styles.btnText}>Repeat</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.btnBG}
            onPress={clickHandlerReveal}>
            <Text style={styles.btnText}>Reveal</Text>
          </TouchableOpacity>
        </View>
      </View>
      

      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={[styles.btnBG, styles.extended]}
          onPress={clickHandlerNext}>
          <Text style={styles.btnText}>Next</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: "flex-end",
    alignItems: 'center',
  },
  top: {
    flex: 1,
    padding: 40,
    alignSelf: "flex-start",
  },
  inversionContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: 'center',
  },
  buttonRow: {
    flexDirection: "row",
    alignItems: 'center',
  },
  containerProgression: {
    margin: 44,
  },
  buttonContainer: {
    marginBottom: 30,
  },
  progressionText: {
    fontWeight: 'bold',
    fontSize: 45,
  },
  keyText: {
    fontWeight: 'bold',
    fontSize: 35,
  },
  inversionText: {
    fontWeight: 'bold',
    fontSize: 25,
  },
  btnBG: {
    margin: 10,
    marginBottom: 7,
    padding: 38,
    backgroundColor: "#b80050",
    borderRadius: 40,
  },
  btnText: {
    color: "white",
    textAlign: 'center',
    fontSize: 28,
  },
  extended: {
    paddingLeft: 130,
    paddingRight: 130,
  },
});