# Changelog

### 0.1.9
Hotfix: Disable rotation

### 0.1.8
Fix: Play only one progression at a fime

### 0.1.7
Fix key not always showing
Added option to use Inversions
Made sure that the key alsways shows correctly
Make sure that inversions are played in a close position

### 0.1.6
Play inversions closer together depending on previous chord

### 0.1.5
Add option to use inversions

### 0.1.4
Removed requirement for Internet permission which was added by default