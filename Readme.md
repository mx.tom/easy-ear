# Easy Ear - Ear Trainer for Chord Progressions #

Easy Ear is a simple Ear Trainer for Chord Progressions. Currently, it provides only basic functionality but more feature will be implemented in the future (see roadmap).

It is written in React Native.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.easyear/)

<img src="metadata/en-US/images/phoneScreenshots/1.png" alt="Screenshot of the app" width="300px" height="600px">

<br><br>

## Roadmap ##

- Completely customize which chords to practice
- Customize length of chord progression
- Other instruments
- Different types of training
- Dark mode

